#!/usr/bin/env python

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import argparse

class pyDriveConnector:
    def __init__(self, folder=None):
        if(folder!= None):
            self.folder = folder
        self.gauth = None
        self.drive = None
        self.folderList = None
        self.fileList = None
    
    def connect(self):
        self.gauth = GoogleAuth()
        #self.gauth.LocalWebserverAuth()
        self.drive = GoogleDrive(self.gauth)

    def listDir(self,driveID,wantPrint=False):
        self.folderList = self.drive.ListFile({'q': "'"+driveID+"'"" in parents and trashed=false", 'corpora': 'teamDrive', 'teamDriveId': driveID, 'includeTeamDriveItems': True, 'supportsTeamDrives': True}).GetList()
        if(wantPrint==True):
            for folder in self.folderList:
                print('title: %s, id: %s' % (folder['title'], folder['id']))

    def listFiles(self,wantPrint=False):
        self.fileList = self.drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
        if(wantPrint==True):
            for file in self.fileList:
                print('title: %s, id: %s, type: %s' % (file['title'], file['id'], file['mimeType']))

    def sendFile(self,driveID,folder,file):
        self.createFolder(driveID,folder)
        for folderElem in self.folderList:
            if folderElem['title'] == folder:
                print("Folder: %s found ID:" % folder, folderElem['id'])
                self.fileToSend = self.drive.CreateFile({'title':file,
                                                        "parents": [{"kind": "drive#fileLink", 'teamDriveId': driveID, "id":folderElem['id']}]})
                self.fileToSend.SetContentFile(file)
                self.fileToSend.Upload(param={'supportsTeamDrives': True})

    def createFolder(self,driveID,wantedFolder):
        self.listDir(driveID)
        self.exist = False
        for folder in self.folderList:
            if(folder['title']==wantedFolder):
                print('Folder: ' + wantedFolder + ' already exists')
                self.exist = True
        if(self.exist == False):
            print('Create Folder')
            self.folder_metadata = {'title' : wantedFolder, 'mimeType' : 'application/vnd.google-apps.folder'}
            self.folder = self.drive.CreateFile(self.folder_metadata)
            self.folder.Upload()

def main():

    # read commandline arguments, first
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True, 
    help="File to send")

    parser.add_argument('-d', '--directory', required=True,
    help="Directory Where file will be send")

    parser.add_argument('-i', '--driveID', required=True,
    help="TeamDrive Root ID (can be found in drive URL")

    args = parser.parse_args()

    connector = pyDriveConnector()
    connector.connect()
    connector.sendFile(args.driveID, args.directory, args.file)
    
    

if __name__=="__main__":
    main()